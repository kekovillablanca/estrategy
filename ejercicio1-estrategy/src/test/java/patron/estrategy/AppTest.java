package patron.estrategy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * Unit test for simple App.
 */
class AppTest {
    /**
     * Rigorous Test.
     */
    @Test
    void correoTest() {
        Envio envio1 = new ColectivosSur(500);
        Envio envio2 = new Andreani(900);
        Envio envio3 = new CorreoArgentino(1000);
        Envio envio4 = new Oca(1500);
        Carrito carrito = new Carrito();
        carrito.agregarProducto(new Producto(500));
        Sistema sistema = new Sistema(envio1, carrito);
        assertEquals(sistema.calcularCosto(), 1000);
        carrito.agregarProducto(new Producto(200));
        sistema.asignarEnvio(envio4);
        assertEquals(sistema.calcularCosto(), 2200);

    }
}
