package patron.estrategy;

public class Producto {

    private double costo;

    public Producto(double costo) {
        this.costo = costo;
    }

    public double costoProducto() {
        return this.costo;
    }
}
