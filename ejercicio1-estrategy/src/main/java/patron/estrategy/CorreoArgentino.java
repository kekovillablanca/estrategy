package patron.estrategy;

public class CorreoArgentino implements Envio {
    private double costo;

    public CorreoArgentino(double costo) {
        this.costo = costo;
    }

    @Override
    public double costoEnvio() {
        return this.costo;
    }

}
