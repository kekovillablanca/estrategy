package patron.estrategy;

public class ColectivosSur implements Envio {
    private double costo;

    public ColectivosSur(double costo) {
        this.costo = costo;
    }

    @Override
    public double costoEnvio() {
        return this.costo;
    }
}
