package patron.estrategy;

public class Andreani implements Envio {
    private double costo;

    public Andreani(double costo) {
        this.costo = costo;
    }

    @Override
    public double costoEnvio() {
        return this.costo;
    }
}
