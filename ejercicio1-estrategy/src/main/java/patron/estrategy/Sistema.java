package patron.estrategy;

public class Sistema {
    private Envio tipoEnvio;
    private Carrito carrito;

    public Sistema(Envio envio, Carrito carrito) {
        this.carrito = carrito;
        this.tipoEnvio = envio;
    }

    public double calcularCosto() {

        return tipoEnvio.costoEnvio() + carrito.obtenerCostoProductos();
    }

    public void asignarEnvio(Envio envio) {
        this.tipoEnvio = envio;
    }
}
