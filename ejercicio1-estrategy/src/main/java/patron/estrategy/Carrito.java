package patron.estrategy;

import java.util.ArrayList;
import java.util.List;

public class Carrito {
    List<Producto> productos = new ArrayList<Producto>();

    public void agregarProducto(Producto productos) {
        this.productos.add(productos);

    }

    public double obtenerCostoProductos() {
        double costoTotal = 0;
        for (Producto prod : this.productos) {
            costoTotal += prod.costoProducto();

        }
        return costoTotal;
    }
}
