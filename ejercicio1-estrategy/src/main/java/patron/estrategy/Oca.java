package patron.estrategy;

public class Oca implements Envio {
    private double costo;

    public Oca(double costo) {
        this.costo = costo;

    }

    @Override
    public double costoEnvio() {
        return this.costo;
    }

}
