package patron.estrategy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

/**
 * Unit test for simple App.
 */
class AppTest {
    /**
     * Rigorous Test.
     */
    @Test
    void fechaNacimientoCortaTest() {
        FechaNacimiento fechaNacimientoCorta = new FechaCorta(LocalDate.now().plusDays(2));

        Persona persona = new Persona(fechaNacimientoCorta);
        assertEquals(persona.fechaNacimiento(), "2022/06/17");
    }

    @Test
    void fechaNacimientoLargaTest() {
        FechaNacimiento fechaNacimientoLarga = new FechaLarga(LocalDate.now().minusDays(2));
        Persona persona = new Persona(fechaNacimientoLarga);
        assertEquals(persona.fechaNacimiento(), "lunes, 13 de junio de 2022");
    }
}
