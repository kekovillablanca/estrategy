package patron.estrategy;

import java.time.LocalDate;

abstract class FechaNacimiento {
    protected LocalDate fecha;

    public FechaNacimiento(LocalDate fecha) {
        this.fecha = fecha;
    }

    public abstract String fecha();
}