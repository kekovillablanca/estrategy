package patron.estrategy;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class FechaCorta extends FechaNacimiento {

    public FechaCorta(LocalDate fecha) {
        super(fecha);

    }

    @Override
    public String fecha() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");

        return this.fecha.format(dtf);

    }

}
