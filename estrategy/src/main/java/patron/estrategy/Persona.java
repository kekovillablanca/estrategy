package patron.estrategy;

public class Persona {
    FechaNacimiento fechaNacimiento;

    public Persona(FechaNacimiento fechaNac) {
        this.fechaNacimiento = fechaNac;
    }

    public String fechaNacimiento() {
        return this.fechaNacimiento.fecha();
    }
}
