package patron.estrategy;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class FechaLarga extends FechaNacimiento {

    public FechaLarga(LocalDate fecha) {
        super(fecha);

    }

    @Override
    public String fecha() {
        DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("EEEE, dd 'de' MMMM 'de' yyyy");

        return this.fecha.format(dtf2);

    }

}
